package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user){
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    @Override
    public ResponseEntity updateUser(Long id, String stringToken, User user) {
        User userForUpdating = userRepository.findById(id).get();
        String userOwner = userForUpdating.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userOwner)){
            userForUpdating.setUsername(user.getUsername());
            userForUpdating.setPassword(user.getPassword());
            userRepository.save(userForUpdating);
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this user", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deleteUser(Long id, String stringToken) {
        User userForUpdating = userRepository.findById(id).get();
        String userOwner = userForUpdating.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userOwner)){
            userRepository.deleteById(id);
            return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to delete this user", HttpStatus.UNAUTHORIZED);
        }
    }
}


